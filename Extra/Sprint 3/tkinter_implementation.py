import pycelonis
import pm4py
from pycelonis.pql import PQL, PQLColumn, PQLFilter, OrderByColumn
import pandas as pd
import warnings
warnings.filterwarnings("ignore")
import tkinter as tk
import subprocess

# establishing connection with Celonis
def get_celonis_values():
    global base_url, api_token, connection, celonis
    base_url = entry1.get()
    api_token = entry2.get()
    #base_url = 'academic-celonis-x509kq.eu-2.celonis.cloud'
    #api_token = 'NTg1YjI0YzUtNjI5Yi00NzRmLWE1ZmQtMWQxYzM3OGRkYWQ0OldNMk5ERkdCUUZER1FJeXdYaTY3dFprOCs5dExOd3M0czRtYXk0dmJyTFhJ'
    try:
        celonis = pycelonis.get_celonis(base_url = base_url,
                                api_token = api_token,
                                key_type = 'USER_KEY')
        result_label.config(text = "Connected to Celonis")
        connection = 1
    except:
        result_label.config(text = "Please check if the URL or API token is valid.")
        connection = 0
    celonis_connection_window.after(1000, close_celonis_connection_window)

def close_celonis_connection_window():
    if connection == 1:
        celonis_connection_window.destroy()
        open_load_log_window()

celonis_connection_window = tk.Tk()
celonis_connection_window.title("Connect with PyCelonis")

label1 = tk.Label(celonis_connection_window, text = "Enter the Celonis URL:")
label1.pack()
entry1 = tk.Entry(celonis_connection_window, width = 30)
entry1.pack()
label2 = tk.Label(celonis_connection_window, text = "Enter the API Token:")
label2.pack()
entry2 = tk.Entry(celonis_connection_window, width = 30)
entry2.pack()

store_button = tk.Button(celonis_connection_window, text="Establish Connection",
                         command = get_celonis_values)
store_button.pack()

result_label = tk.Label(celonis_connection_window, text="")
result_label.pack()

# loading the dataset
from tkinter import filedialog

def open_load_log_window():
    global load_log_window
    load_log_window = tk.Tk()
    load_log_window.title("Load the Event Log")

    load_log_button = tk.Button(load_log_window, text="Load XES", command = load_xes)
    load_log_button.pack()

    global log_result_label
    log_result_label = tk.Label(load_log_window, text="")
    log_result_label.pack(pady=(0, 10))

    global display_log_button
    display_log_button = tk.Button(load_log_window, text = "Show the Event Log", command=lambda: display_df(data))
    

    global start_data_int_button
    start_data_int_button = tk.Button(load_log_window, text = "Go to Data Integration", command = go_to_data_int)
    
    #load_log_window.mainloop()

def load_xes():
    global data_loaded
    log_result_label.config(text="Loading...")
    load_log_window.update()
    file_path = filedialog.askopenfilename(filetypes=[("XES Files", "*.xes")])
    if file_path:
        try:
            global original_data, data
            original_data = pm4py.read_xes(file_path)
            log_result_label.config(text = "XES file loaded successfully")
            data_loaded = 1

            if len(original_data) < 100000:
                data = original_data.copy() 
            else:
                data = original_data[:100000].copy()
            
        except Exception:
            log_result_label.config(text = "Error loading XES file")
            data_loaded = 0

    if data_loaded == 1:
        display_log_button.pack(pady=(0, 10))
        start_data_int_button.pack()

import os
def display_df(dataframe):
    dataframe.to_csv("output.csv", index = False)
    os.startfile("output.csv")

def go_to_data_int():
    load_log_window.destroy()
    open_data_int_window()

# data integration
def open_data_int_window():
    global data_int_window
    data_int_window = tk.Tk()
    data_int_window.title("Data Integration")

    data_pool_label = tk.Label(data_int_window, text = "Enter the Data Pool name:")
    data_pool_label.pack()
    data_pool_available = tk.Label(data_int_window, text = "Available Data Pools: " + str([dp.name for dp in celonis.data_integration.get_data_pools()]))
    data_pool_available.pack()
    global data_pool_entry
    data_pool_entry = tk.Entry(data_int_window, width = 30)
    data_pool_entry.pack()
    global data_pool_button
    data_pool_button = tk.Button(data_int_window, text = "Create/Get the Data Pool", command = get_data_pool)
    data_pool_button.pack()
    global data_pool_result_label
    data_pool_result_label = tk.Label(data_int_window, text = "")
    data_pool_result_label.pack(pady=(0, 10))

    data_model_label = tk.Label(data_int_window, text = "Enter the Data Model name:")
    data_model_label.pack()
    global data_model_available
    data_model_available = tk.Label(data_int_window, text ="")
    data_model_available.pack()
    global data_model_entry
    data_model_entry = tk.Entry(data_int_window, width = 30)
    data_model_entry.pack()
    global data_model_button
    data_model_button = tk.Button(data_int_window, text = "Create/Get the Data Model", command = get_data_model, state = tk.DISABLED)
    data_model_button.pack()
    global data_model_result_label
    data_model_result_label = tk.Label(data_int_window, text = "")
    data_model_result_label.pack(pady=(0, 10))

    data_table_label = tk.Label(data_int_window, text = "Enter the Data Table name:")
    data_table_label.pack()
    global data_table_available
    data_table_available = tk.Label(data_int_window, text ="")
    data_table_available.pack()
    global data_table_entry
    data_table_entry = tk.Entry(data_int_window, width = 30)
    data_table_entry.pack()
    global data_table_button
    data_table_button = tk.Button(data_int_window, text = "Create/Get the Data Table", command = get_data_table, state = tk.DISABLED)
    data_table_button.pack()
    global data_table_result_label
    data_table_result_label = tk.Label(data_int_window, text = "")
    data_table_result_label.pack(pady=(0, 10))

    global process_configuration_button
    process_configuration_button = tk.Button(data_int_window, text = "Configure Process", command = process_configuration, state = tk.DISABLED)
    process_configuration_button.pack()
    global process_configuration_result_label
    process_configuration_result_label = tk.Label(data_int_window, text="")
    process_configuration_result_label.pack(pady=(0, 10))

    global data_reload_button
    data_reload_button = tk.Button(data_int_window, text = "Reload Data", command = data_reload, state = tk.DISABLED)
    data_reload_button.pack()
    global data_reload_result_label
    data_reload_result_label = tk.Label(data_int_window, text="")
    data_reload_result_label.pack(pady=(0, 10))

    global start_menu_button
    start_menu_button = tk.Button(data_int_window, text = "Go to Analysis Menu", command = go_to_menu, state = tk.DISABLED)
    start_menu_button.pack()

    #open_data_int_window.mainloop()
    
def get_data_pool():
    global data_pool_name, data_pool
    data_pool_name = data_pool_entry.get() #data_pool
    try:
        data_pool = celonis.data_integration.get_data_pools().find(data_pool_name)
        data_pool_result_label.config(text = "The data pool already exists and has been fetched successfully.")
    except:
        data_pool = celonis.data_integration.create_data_pool(data_pool_name)  
        data_pool_result_label.config(text = "Data pool has been successfully created.")
    data_model_button.config(state = tk.NORMAL)
    data_model_available.config(text="Available Data Models: " + str([dm.name for dm in data_pool.get_data_models()]))
    data_int_window.update()

def get_data_model():
    global data_model_name, data_model
    data_model_name = data_model_entry.get() #data_model
    try:
        data_model = data_pool.get_data_models().find(data_model_name)
        data_model_result_label.config(text = "The data model already exists and has been fetched successfully.")
    except:
        data_model = data_pool.create_data_model(data_model_name)  
        data_model_result_label.config(text = "Data model has been successfully created.")
    data_table_button.config(state = tk.NORMAL)
    data_table_available.config(text = "Available Data Tables: " + str([dt.name for dt in data_pool.get_tables()]))
    data_int_window.update()

def get_data_table():
    global data_table_name, data_table, tables
    data_table_result_label.config(text="Loading...")
    data_int_window.update()
    data_table_name = data_table_entry.get() #data_table
    try:
        data_pool.create_table(df = data, table_name = data_table_name,  drop_if_exists = False)
        data_table_result_label.config(text = "Data table has been successfully created.")
    except:
        data_pool.get_tables()  
        data_table_result_label.config(text = "The data table already exists and has been fetched successfully.")

    try:
        tables = data_model.add_table(name = data_table_name, alias = data_table_name)
    except:
        pass

    data_reload_button.config(state = tk.NORMAL)
    process_configuration_button.config(state = tk.NORMAL)

def data_reload():
    data_reload_result_label.config(text="In progress...")
    data_int_window.update()
    try:
        data_model.reload()
        data_reload_result_label.config(text = "Data successfuly reloaded.")
    except:
        data_reload_result_label.config(text = "Issue with Data Reload. Try recreating from scratch.")

    

def process_configuration():
    try:
        tables = data_model.get_tables()
        activities = tables.find(data_table_name)
        process_configuration = data_model.create_process_configuration(activity_table_id = activities.id,
                                                                        case_id_column = "case:concept:name",
                                                                        activity_column = "concept:name",
                                                                        timestamp_column = "time:timestamp",)
        process_configuration_result_label.config(text = "Process Configuration successful.")
    except:
        process_configuration_result_label.config(text = "Process Configuration unsuccessful.")
    start_menu_button.config(state = tk.NORMAL)

def go_to_menu():
    data_int_window.destroy()
    open_menu_window()

# Menu
def open_menu_window():
    global menu_window
    menu_window = tk.Tk()
    menu_window.title("Functionality Menu")

    choose_analysis_label = tk.Label(menu_window, text = "Choose an analysis:")
    choose_analysis_label.pack(pady=(0, 10))

    #global batches_button
    batches_button = tk.Button(menu_window, text = "Batch Identification", command = batch_identification)
    batches_button.pack(pady=(0, 10))

    #global repeated_act_button
    repeated_act_button = tk.Button(menu_window, text = "Activities Repeated by Different Resources", command = repeated_act)
    repeated_act_button.pack(pady=(0, 10))

    #global res_act_perf_button
    res_act_perf_button = tk.Button(menu_window, text = "Resource-Activity Performance", command = res_act_perf)
    res_act_perf_button.pack(pady=(0, 10))

    #global dev_in_wp_button
    dev_in_wp_button = tk.Button(menu_window, text = "Deviation in Work Pattern", command = dev_in_work_pat)
    dev_in_wp_button.pack(pady=(0, 10))
    
    dev_in_coll_patt_button = tk.Button(menu_window, text = "Deviation in Collaboration Patterns", command = dev_in_coll_patt)
    dev_in_coll_patt_button.pack()

# Batch Identification
def batch_identification():
    batch_id_window = tk.Tk()
    batch_id_window.title("Batch Identification")

    global dataframe
    data = celonis.data_integration
    data_pool = data.get_data_pools().find(data_pool_name)
    data_model = data_pool.get_data_models().find(data_model_name)
    pql = PQL()
    pql.columns.append(PQLColumn(name="Case ID", query="\"data_table\".\"CASE:CONCEPT:NAME\""))
    pql.columns.append(PQLColumn(name="Activity", query='"data_table"."concept:name"'))
    pql.columns.append(PQLColumn(name="Resource", query='"data_table"."ORG:RESOURCE"'))
    pql.columns.append(PQLColumn(name="Time", query='"data_table"."time:timestamp"'))
    dataframe = data_model.export_data_frame(pql)

    display_df_button = tk.Button(batch_id_window, text = "Display the Data extracted from Celonis", command = lambda: display_df(dataframe))
    display_df_button.pack()

    batches_label = tk.Label(batch_id_window, text = "Enter the merge distance (minutes)")
    batches_label.pack()
    global batches_entry
    batches_entry = tk.Entry(batch_id_window, width = 30)
    batches_entry.pack()
    batches_button = tk.Button(batch_id_window, text="Show Batches", command = find_batches)
    batches_button.pack()
    global batches_result_label
    batches_result_label = tk.Label(batch_id_window, text="")
    batches_result_label.pack()    

def find_batches():
    global dataframe
    distance = batches_entry.get()
    batches = pm4py.discover_batches(dataframe, case_id_key='Case ID',
                                     resource_key = 'Resource', activity_key = 'Activity',
                                     timestamp_key = 'Time', merge_distance = int(distance)*60)
    batches_df = pd.DataFrame(columns = ["Activity", "Resource", "No. of Batches",
                                         "Concurrent batching", "Simultaneous", "Batching at start",
                                         "Batching at end", "Sequential batching"])
    for activity_resource in batches:
        record = {}
        record["Activity"] = activity_resource[0][0]
        record["Resource"] = activity_resource[0][1]
        record["No. of Batches"] = activity_resource[1]
        for batch_type in activity_resource[2]:
            record[batch_type] = len(activity_resource[2][batch_type])
        batches_df = batches_df._append(record, ignore_index=True)
    batches_df = batches_df.fillna(0)
    display_df(batches_df)
    batches_result_label.config(text = "Number of batches: " + str(batches_df['No. of Batches'].sum()))

# Activities Repeated by Different Resources
def repeated_act():
    repeated_act_window = tk.Tk()
    repeated_act_window.title("Activities Repeated by Different Resources")

    global dataframe
    pql = PQL()
    pql.columns.append(PQLColumn(name="Activity", query='"data_table"."concept:name"'))
    pql.columns.append(PQLColumn(name="Resource", query='"data_table"."org:resource"'))
    pql.columns.append(PQLColumn(name="Case ID", query='"data_table"."case:concept:name"'))
    dataframe = data_model.export_data_frame(pql)

    display_df_button = tk.Button(repeated_act_window, text = "Display the Data extracted from Celonis", command = lambda: display_df(dataframe))
    display_df_button.pack(pady=(0, 10))

    act_in_cases_by_res_button = tk.Button(repeated_act_window, text="Show Resources for All Activities in Every Case", command = find_act_in_cases_by_res)
    act_in_cases_by_res_button.pack()

    global final_result_button
    final_result_button = tk.Button(repeated_act_window, text="Show the Number of Resources for All Activities in Every Case", command = final_result_act, state = tk.DISABLED)
    final_result_button.pack()

def find_act_in_cases_by_res():
    global dataframe
    dataframe['Different Activities for All Cases'] = 'Case ID: '+ dataframe['Case ID'] + ' (activity: ' + dataframe['Activity'] + ')'
    global res_df
    res_df = dataframe.groupby(['Different Activities for All Cases'])['Resource'].apply(list).to_frame()
    res_df.reset_index(inplace=True)
    display_df(res_df)

    final_result_button.config(state = tk.NORMAL)

def final_result_act():
    global res_d
    num=[]
    def filter_resource():
        res=[]
        for l1 in  res_df['Resource']:
            l2=list(filter(None, l1))
            res.append(l2)
            num.append(len(l2))
        return res   
    res_df['flitered resources'] = filter_resource()
    res_df['Number of resources'] = num
    display_df(res_df)

# Resource-activity Performance   
def res_act_perf():
    res_act_perf_window = tk.Tk()
    res_act_perf_window.title("Resource-Activity Performance")

    global dataframe #xyz
    pql = PQL()
    pql.columns.append(PQLColumn(name="Case_id", query='"data_table_CASES"."case:concept:name"'))
    pql.columns.append(PQLColumn(name="Previous_activity", query='SOURCE("data_table"."CONCEPT:NAME")'))
    pql.columns.append(PQLColumn(name="Current_Activity", query='TARGET("data_table"."CONCEPT:NAME")'))
    pql.columns.append(PQLColumn(name="Status", query='TARGET("data_table"."lifecycle:transition")'))
    pql.columns.append(PQLColumn(name="Resource", query='TARGET("data_table"."org:resource")'))
    pql.columns.append(PQLColumn(name="Throughput_Time_sec", query='SECONDS_BETWEEN(SOURCE("data_table"."TIME:TIMESTAMP"), TARGET("data_table"."TIME:TIMESTAMP"))'))

    dataframe = data_model.export_data_frame(pql)
    display_df_button = tk.Button(res_act_perf_window, text = "Display the Data extracted from Celonis", command = lambda: display_df(dataframe))
    display_df_button.pack()

    mean_times = dataframe.groupby(['Current_Activity', 'Resource'])['Throughput_Time_sec'].mean().reset_index()
    activities_name = mean_times['Current_Activity'].unique()
    min_dataframes = []
    max_dataframes = []
    for activity in activities_name:
        subset = mean_times[mean_times['Current_Activity'] == activity]
        min_times = subset[subset['Throughput_Time_sec'] == subset['Throughput_Time_sec'].min()]
        max_times = subset[subset['Throughput_Time_sec'] == subset['Throughput_Time_sec'].max()]
        min_times_each_activity = pd.DataFrame(min_times)
        max_times_each_activity = pd.DataFrame(max_times)
        min_dataframes.append(min_times_each_activity)
        max_dataframes.append(max_times_each_activity)

    most_efficient_resources_per_activity = pd.concat(min_dataframes)
    least_efficient_resources_per_activity = pd.concat(max_dataframes)
    
    most_eff_button = tk.Button(res_act_perf_window, text = "Show Most Efficient Resources", command = lambda: display_df(most_efficient_resources_per_activity))
    most_eff_button.pack()

    least_eff_button = tk.Button(res_act_perf_window, text = "Show Least Efficient Resources", command = lambda: display_df(least_efficient_resources_per_activity))
    least_eff_button.pack()

# Deviations in the Work Pattern
def dev_in_work_pat():
    dev_in_work_pat_window = tk.Tk()
    dev_in_work_pat_window.title("Deviations in the Work Pattern")
    
    pql = PQL()
    pql.columns.append(PQLColumn(name="Case ID", query="\"data_table\".\"CASE:CONCEPT:NAME\""))
    pql.columns.append(PQLColumn(name="Activity", query='"data_table"."concept:name"'))
    pql.columns.append(PQLColumn(name="Resource", query='"data_table"."ORG:RESOURCE"'))
    pql.columns.append(PQLColumn(name="Time", query='"data_table"."time:timestamp"'))

    global dataframe
    dataframe = data_model.export_data_frame(pql)
    display_df_button = tk.Button(dev_in_work_pat_window, text = "Display the Data extracted from Celonis", command = lambda: display_df(dataframe))
    display_df_button.pack(pady=(0, 10))

    four_eyes_button = tk.Button(dev_in_work_pat_window, text = "4 Eyes Principle", command = four_eyes)
    four_eyes_button.pack(pady=(0, 10))

    activity_done_different_resources_button = tk.Button(dev_in_work_pat_window, text = "Activities done by Different Resources",
                                                         command = activity_done_different_resources)
    activity_done_different_resources_button.pack(pady=(0, 10))

    uncommon_roles_button = tk.Button(dev_in_work_pat_window, text = "Analyse Uncommmon Roles", command = uncommon_roles)
    uncommon_roles_button.pack()
    global uncommon_roles_label
    uncommon_roles_label = tk.Label(dev_in_work_pat_window, text = "")
    uncommon_roles_label.pack() 

def four_eyes():
    global four_eyes_window
    four_eyes_window = tk.Tk()
    four_eyes_window.title("4 Eyes Principle")

    global dataframe, activities
    activities = dataframe["Activity"].unique()
    num_act_label = tk.Label(four_eyes_window, text = "Number of activities: " + str(len(activities)))
    num_act_label.pack()

    global activity_pairs
    activity_pairs = []
    for i in range(len(activities)):
        for j in range(i+1, len(activities)):
            activity_pairs.append((activities[i], activities[j]))
    num_act_pairs_label = tk.Label(four_eyes_window, text = "Number of activity pairs: " + str(len(activity_pairs)))
    num_act_pairs_label.pack(pady=(0, 10))    

    global cases_for_act_pair_label
    cases_for_act_pair_button = tk.Button(four_eyes_window, text="See Violating Cases for Every Activity Pair", command = cases_for_act_pair)
    cases_for_act_pair_button.pack()
    cases_for_act_pair_label = tk.Label(four_eyes_window, text = "")
    cases_for_act_pair_label.pack()

    global res_act_pair_case_label
    res_act_pair_case_label = tk.Label(four_eyes_window, text = "WARNING! This might take a long time to run for bigger datasets.")
    res_act_pair_case_label.pack()
    global res_act_pair_case_button
    res_act_pair_case_button = tk.Button(four_eyes_window, text="See Resources executing each Activity Pair under each Case",
                                         command = res_act_pair_case, state = tk.DISABLED)
    res_act_pair_case_button.pack()
    


    
def cases_for_act_pair():
    global four_eyes_df, dataframe
    cases_for_act_pair_label.configure(text = "Loading...")
    four_eyes_window.update()
    four_eyes_df = pd.DataFrame(columns = ["Activity 1", "Activity 2", "# of Cases Violating 4 Eyes",
                                           "List of Cases"])
    global four_eyes_df2
    four_eyes_df2 = four_eyes_df.copy()
    for activity_pair in activity_pairs:
        filtered = pm4py.filter_four_eyes_principle(dataframe, case_id_key = "Case ID", activity_key = "Activity",
                                                    timestamp_key = "Time", resource_key = "Resource",
                                                    activity1 = activity_pair[0], 
                                                    activity2 = activity_pair[1])
        if len(filtered["Case ID"].unique()) > 0:
            new_record = {"Activity 1":activity_pair[0], "Activity 2":activity_pair[1],
                          "# of Cases Violating 4 Eyes":len(filtered["Case ID"].unique()),
                          "List of Cases":filtered["Case ID"].unique()}
            four_eyes_df = four_eyes_df._append(new_record, ignore_index=True)

        new_record = {"Activity 1":activity_pair[0], "Activity 2":activity_pair[1],
                          "# of Cases Violating 4 Eyes":len(filtered["Case ID"].unique()),
                          "List of Cases":filtered["Case ID"].unique()}
        four_eyes_df2 = four_eyes_df2._append(new_record, ignore_index=True)

    display_df(four_eyes_df)
    cases_for_act_pair_label.configure(text = "")
    four_eyes_window.update()
    res_act_pair_case_button.config(state = tk.NORMAL)

def res_act_pair_case():
    global four_eyes_df, dataframe

    res_act_pair_case_label.configure(text = "Running...")
    four_eyes_window.update()
    
    res_four_eyes_df = pd.DataFrame(columns = ["Case ID","Activity 1", "Activity 2", "The Resource executing Activity 1",
                                               "The Resource executing Activity 2"])
    for activity_pair in activity_pairs:
        list_of_case = four_eyes_df2[(four_eyes_df2['Activity 1']== activity_pair[0]) & ( four_eyes_df2['Activity 2'] == activity_pair[1])]['List of Cases'].iloc[0]
        for case in list_of_case:
            df_case = dataframe[(dataframe['Case ID'] == case)]
            resource_1 = df_case[df_case['Activity'] == activity_pair[0]]['Resource'].iloc[0]
            resource_2 = df_case[df_case['Activity'] == activity_pair[1]]['Resource'].iloc[0]
            new_record = {"Case ID": case, "Activity 1": activity_pair[0],
                          "Activity 2": activity_pair[1], "The Resource executing Activity 1": resource_1, 
                          "The Resource executing Activity 2": resource_2}
            res_four_eyes_df = res_four_eyes_df._append(new_record, ignore_index = True)
    display_df(res_four_eyes_df)
    res_act_pair_case_label.configure(text="")
    four_eyes_window.update()

def activity_done_different_resources():
    global activity_done_different_resources_window
    activity_done_different_resources_window = tk.Tk()
    activity_done_different_resources_window.title("Activity Done by Different Resource")

    activity_done_different_resources_df_button = tk.Button(activity_done_different_resources_window,
                                                            text = "See the Cases where the Activity is done by Diffferent Resources",
                                                            command = activity_done_different_resources_df)
    activity_done_different_resources_df_button.pack()

    activity_done_different_resources_txt_label = tk.Label(activity_done_different_resources_window, text = "Enter the activity for which you want to see the different resources executing it")
    activity_done_different_resources_txt_label.pack()
    global list_of_activities_label
    list_of_activities_label = tk.Label(activity_done_different_resources_window, text="")
    list_of_activities_label.pack()
    global activity_done_different_resources_txt_entry #T07-2 Draft intern advice aspect 2
    activity_done_different_resources_txt_entry = tk.Entry(activity_done_different_resources_window, width = 30)
    activity_done_different_resources_txt_entry.pack()
    activity_done_different_resources_txt_button = tk.Button(activity_done_different_resources_window,
                                                            text = "Show the Different Resources Executing this Activity",
                                                            command = activity_done_different_resources_txt)
    activity_done_different_resources_txt_button.pack()

    

def activity_done_different_resources_df():
    diff_act_df = pd.DataFrame(columns = ["Activity", "# of Cases where the Activity is done by Different Resources",
                                          "List of Cases"])

    global dataframe
    activities = dataframe["Activity"].unique()
    for activity in activities:
        filtered = pm4py.filter_activity_done_different_resources(dataframe, case_id_key = "Case ID", 
                                                                  activity_key = "Activity",
                                                                  timestamp_key = "Time", 
                                                                  resource_key = "Resource",
                                                                  activity = activity)
        if len(filtered["Case ID"].unique()) > 0:
            new_record = {"Activity":activity,
                          "# of Cases where the Activity is done by Different Resources":len(filtered["Case ID"].unique()),
                          "List of Cases":filtered["Case ID"].unique()}
            diff_act_df = diff_act_df._append(new_record, ignore_index = True)
    avail_activities = diff_act_df["Activity"].to_list()
    list_of_activities_label.configure(text = "Available activities: \n" + '\n'.join(avail_activities))
    activity_done_different_resources_window.update()
    display_df(diff_act_df)

def activity_done_different_resources_txt():
    global dataframe
    
    activity_name = activity_done_different_resources_txt_entry.get()
    filtered = pm4py.filter_activity_done_different_resources(dataframe,case_id_key = "Case ID", 
                                                              activity_key = "Activity",
                                                              timestamp_key = "Time", 
                                                              resource_key = "Resource",
                                                              activity = activity_name)

    file_path = "activity_done_different_resources_txt.txt"
    with open(file_path, "w") as file:
        temp = "For Activity: " + str(activity_name) + "\n"
        file.write(temp)
        temp = "The resources executing it throughout different cases:" + str(filtered.loc[(filtered["Activity"] == activity_name)]["Resource"].unique().tolist()) + "\n"
        file.write(temp)
        file.write("\n")

        temp = "\nCase wise execution by different resources:" + "\n"
        file.write(temp)
        cases = filtered["Case ID"].unique()
        for case in cases:
            temp = "\nFor Case ID: " + str(case) + "\n"
            file.write(temp)
            temp_df = filtered.loc[(filtered["Case ID"] == case) & (filtered["Activity"] == activity_name)]
            temp = "The different resources executing it are: " + str(temp_df["Resource"].unique())
            file.write(temp)

    subprocess.call(["notepad", file_path])

def uncommon_roles():

    roles_df = pd.DataFrame(columns = ["Role ID", "# of Activities", "# of Resources",
                                       "Resource(s) with Most Freq.", "Most Frequent Value",
                                       "Resource(s) with Least Freq.", "Least Frequent Value"])
    roles = pm4py.discover_organizational_roles(dataframe, case_id_key = "Case ID",
                                                activity_key = "Activity", timestamp_key = "Time",
                                                resource_key = "Resource")
    role_id = 1
    for role in roles:
        
        highest_frequency = max(role.originator_importance.values())
        resources_with_most_frequency = [key for key, value in role.originator_importance.items() if value == highest_frequency]
        least_frequency = min(role.originator_importance.values())
        resources_with_least_frequency = [key for key, value in role.originator_importance.items() if value == least_frequency]
        
        new_record = {"Role ID": role_id, "# of Activities": len(role.activities),
                      "# of Resources": len(role.originator_importance),
                      "Resource(s) with Most Freq.": resources_with_most_frequency,
                      "Most Frequent Value": highest_frequency,
                      "Resource(s) with Least Freq.": resources_with_least_frequency,
                      "Least Frequent Value": least_frequency}
        roles_df = roles_df._append(new_record, ignore_index = True)
        role_id = role_id + 1
    
    uncommon_roles_label.config(text = "Number of roles indentified: " + str(len(roles)))
    display_df(roles_df)

# Deviations in the Collaboration Patterns
def dev_in_coll_patt():
    dev_in_coll_patt_window = tk.Tk()
    dev_in_coll_patt_window.title("Deviations in the Collaboration Patterns")

    pql = PQL()
    pql.columns.append(PQLColumn(name="case:concept:name", query="\"data_table\".\"CASE:CONCEPT:NAME\""))
    pql.columns.append(PQLColumn(name="concept:name", query='"data_table"."concept:name"'))
    pql.columns.append(PQLColumn(name="org:resource", query='"data_table"."org:resource"'))
    pql.columns.append(PQLColumn(name="time:timestamp", query='"data_table"."time:timestamp"'))
    global dataframe_metrics
    dataframe_metrics = data_model.export_data_frame(pql)

    display_df_button = tk.Button(dev_in_coll_patt_window, text = "Display the Data extracted from Celonis", command = lambda: display_df(dataframe_metrics))
    display_df_button.pack()

    def collab_pattern_sna_generate(data, collaboration_patterns, resource_key = 'org:resource',
                                timestamp_key = 'time:timestamp', case_id_key = 'case:concept:name'):
        global sna
        if collaboration_patterns == "handover of work":
            sna = pm4py.discover_handover_of_work_network(data, resource_key = resource_key, 
                                                          timestamp_key = timestamp_key, case_id_key = case_id_key)
        elif collaboration_patterns == "subcontracting":
            sna = pm4py.discover_subcontracting_network(data, resource_key = resource_key, 
                                                        timestamp_key = timestamp_key, case_id_key = case_id_key)
        elif collaboration_patterns == "working together":
            sna = pm4py.discover_working_together_network(data, resource_key = resource_key,
                                                          timestamp_key = timestamp_key, case_id_key = case_id_key)
        return sna

    def collab_pattern_resource_ranking(data, collaboration_patterns, resource_key='org:resource',
                                    timestamp_key = 'time:timestamp', case_id_key = 'case:concept:name'):
        sna = collab_pattern_sna_generate(data, collaboration_patterns, resource_key = resource_key,
                                          timestamp_key = timestamp_key, case_id_key = case_id_key)
        to_df = pd.DataFrame.from_dict(sna.connections, orient = 'index', columns = ['Value'])
        resource_ranking = to_df.sort_values(by = 'Value', ascending = False)
        resource_ranking.reset_index(level=0, inplace=True)
        display_df(resource_ranking)

    def collab_pattern_resource_matrix(data, collaboration_patterns, resource_key = 'org:resource',
                                   timestamp_key = 'time:timestamp', case_id_key = 'case:concept:name'):
        sna = collab_pattern_sna_generate(data, collaboration_patterns, resource_key = resource_key,
                                          timestamp_key = timestamp_key, case_id_key = case_id_key)   
        resources = set()
        for key in sna.connections.keys():
            resources.add(key[0])
            resources.add(key[1])
        resource_names = list(resources)
        resource_matrix = pd.DataFrame(index=resource_names, columns=resource_names)
        for key, value in sna.connections.items():
            resource_matrix.loc[key[0], key[1]] = value
        resource_matrix.reset_index(level=0, inplace=True)
        display_df(resource_matrix)

    global collaboration_patterns

    collab_pattern_label = tk.Label(dev_in_coll_patt_window, text = "Select a Collaboration Pattern")
    collab_pattern_label.pack()
    #global collab_pattern_entry
    #collab_pattern_entry = tk.Entry(dev_in_coll_patt_window, width = 30)
    #collab_pattern_entry.pack()
    options = ["handover of work", "subcontracting", "working together"]
    global dropdown_var
    dropdown_var = tk.StringVar()
    dropdown_var.set(options[0])
    dropdown = tk.OptionMenu(dev_in_coll_patt_window, dropdown_var, *options)
    dropdown.pack()
    collab_pattern_button = tk.Button(dev_in_coll_patt_window, text = "Select", command = select_collab_pattern)
    collab_pattern_button.pack()
    global collab_pattern_result_label
    collab_pattern_result_label = tk.Label(dev_in_coll_patt_window, text = "")
    collab_pattern_result_label.pack()

    global ranking_button, matrix_button, visualise_button 
    ranking_button = tk.Button(dev_in_coll_patt_window, text = "Show Ranking between Resources",
                               command = lambda: collab_pattern_resource_ranking(dataframe_metrics, collaboration_patterns), state = tk.DISABLED)
    ranking_button.pack()
    matrix_button = tk.Button(dev_in_coll_patt_window, text = "Show Matrix between Resources",
                            command = lambda: collab_pattern_resource_matrix(dataframe_metrics, collaboration_patterns), state = tk.DISABLED)
    matrix_button.pack()
    visualise_button = tk.Button(dev_in_coll_patt_window, text = "Visualise the SNA",
                                 command = lambda: visualise(dataframe_metrics, collaboration_patterns), state = tk.DISABLED)
    #visualise_button.pack()

def select_collab_pattern():
    global collaboration_patterns, dropdown_var
    
    #collaboration_patterns = collab_pattern_entry.get()
    collaboration_patterns = dropdown_var.get()
    if collaboration_patterns not in ["handover of work", "subcontracting", "working together"]:
        collab_pattern_result_label.config(text = "Invalid input. Enter one of these: handover of work, subcontracting or working together")
    else:
        #collab_pattern_result_label.config(text = "Collaboration Pattern Accepted")
        ranking_button.config(state = tk.NORMAL)
        matrix_button.config(state = tk.NORMAL)
        visualise_button.config(state = tk.NORMAL)

def visualise(dataframe_metrics, collaboration_patterns):
        
    #sna = collab_pattern_sna_generate(dataframe_metrics, collaboration_patterns)
    global sna
    pm4py.view_sna(sna)

celonis_connection_window.mainloop()







































