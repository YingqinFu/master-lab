# Deviations in the Work Pattern

# get the data, data pool and data model from Celonis (they are created in the Data_Integration)
data_pool = celonis.data_integration.get_data_pools().find("data_pool")
data_model = data_pool.get_data_models().find("data_model")

# create a PQL object
pql = PQL()

# extract these columns
pql.columns.append(PQLColumn(name="Case ID", query="\"data_table\".\"CASE:CONCEPT:NAME\""))
pql.columns.append(PQLColumn(name="Activity", query='"data_table"."concept:name"'))
pql.columns.append(PQLColumn(name="Resource", query='"data_table"."ORG:RESOURCE"'))
pql.columns.append(PQLColumn(name="Time", query='"data_table"."time:timestamp"'))

# load the columns to a dataframe
dataframe = data_model.export_data_frame(pql)

# Four eyes principle

# Get the number of all different activities
activities = dataframe["Activity"].unique()

# making activity pairs
activity_pairs = []
for i in range(len(activities)):
    for j in range(i+1, len(activities)):
        activity_pairs.append((activities[i], activities[j]))

# creating dataframe to show the results
four_eyes_df = pd.DataFrame(columns = ["Activity 1", "Activity 2", "# of Cases Violating 4 Eyes",
                                       "List of Cases"])

# applying four eyes principle for every activity pair
for activity_pair in activity_pairs:
    filtered = pm4py.filter_four_eyes_principle(dataframe,case_id_key = "Case ID", activity_key = "Activity",
                                                timestamp_key = "Time", resource_key = "Resource",
                                                activity1 = activity_pair[0], 
                                                activity2 = activity_pair[1])
    new_record = {"Activity 1":activity_pair[0], "Activity 2":activity_pair[1],
                  "# of Cases Violating 4 Eyes":len(filtered["Case ID"].unique()),
                  "List of Cases":filtered["Case ID"].unique()}
    four_eyes_df = four_eyes_df.append(new_record, ignore_index=True)

# for better visualization of results, we creat a dataframe to show the resources 
# executing each activity pair under each case.
res_four_eyes_df = pd.DataFrame(columns = ["Case ID","Activity 1", "Activity 2", "The Resource executing Activity 1",
                                           "The Resource executing Activity 2"])

for activity_pair in activity_pairs:
    
    # all possible cases with the two activities
    list_of_case=four_eyes_df[(four_eyes_df['Activity 1']== activity_pair[0]) & ( four_eyes_df['Activity 2']== activity_pair[1])]['List of Cases'].iloc[0]
    
    for case in list_of_case:
        
        # get the resources in each case for the two activities
        df_case = dataframe[(dataframe['Case ID'] == case)]
        resource_1 = df_case[df_case['Activity'] == activity_pair[0]]['Resource'].iloc[0]
        resource_2 = df_case[df_case['Activity'] == activity_pair[1]]['Resource'].iloc[0]
        new_record = {"Case ID": case, "Activity 1": activity_pair[0],
                      "Activity 2": activity_pair[1], "The Resource executing Activity 1": resource_1, 
                      "The Resource executing Activity 2": resource_2}
        res_four_eyes_df = res_four_eyes_df.append(new_record, ignore_index = True)

# Activities done by different resources

# creating dataframe to show the results
diff_act_df = pd.DataFrame(columns = ["Activity", "# of Cases where the Activity is done by Different Resources",
                                      "List of Cases"])

for activity in activities:
    filtered = pm4py.filter_activity_done_different_resources(dataframe,case_id_key = "Case ID", 
                                                              activity_key = "Activity",
                                                              timestamp_key = "Time", 
                                                              resource_key = "Resource",
                                                              activity = activity)
    new_record = {"Activity":activity,
                  "# of Cases where the Activity is done by Different Resources":len(filtered["Case ID"].unique()),
                  "List of Cases":filtered["Case ID"].unique()}
    diff_act_df = diff_act_df.append(new_record, ignore_index=True)

# for a particular activity, see what are the different resources executing it

# choose an activity name for analysis
activity_name = "T07-2 Draft intern advice aspect 2"

filtered = pm4py.filter_activity_done_different_resources(dataframe,case_id_key = "Case ID", 
                                                              activity_key = "Activity",
                                                              timestamp_key = "Time", 
                                                              resource_key = "Resource",
                                                              activity = activity_name)

cases = filtered["Case ID"].unique()
for case in cases:
    print("\nFor Case ID:", case)
    temp_df = filtered.loc[(filtered["Case ID"] == case) & (filtered["Activity"] == activity_name)]
    print("The different resources executing it are:", temp_df["Resource"].unique())


# Analyzing uncommon resources in roles

roles_df = pd.DataFrame(columns = ["Role ID", "# of Activities", "# of Resources",
                                   "Resource(s) with Most Freq.", "Most Frequent Value",
                                   "Resource(s) with Least Freq.", "Least Frequent Value"])

roles = pm4py.discover_organizational_roles(dataframe, case_id_key = "Case ID",
                                            activity_key = "Activity", timestamp_key = "Time",
                                            resource_key = "Resource")

role_id = 1
for role in roles:
    
    highest_frequency = max(role.originator_importance.values())
    resources_with_most_frequency = [key for key, value in role.originator_importance.items() if value == highest_frequency]
    least_frequency = min(role.originator_importance.values())
    resources_with_least_frequency = [key for key, value in role.originator_importance.items() if value == least_frequency]
    
    new_record = {"Role ID": role_id, "# of Activities": len(role.activities),
                  "# of Resources": len(role.originator_importance),
                  "Resource(s) with Most Freq.": resources_with_most_frequency,
                  "Most Frequent Value": highest_frequency,
                  "Resource(s) with Least Freq.": resources_with_least_frequency,
                  "Least Frequent Value": least_frequency}
    roles_df = roles_df.append(new_record, ignore_index = True)
    role_id = role_id + 1
